# Test Web Component Map

This repo tests developing with the openlayers-elements
Web Component map.

## Run Development

Details to link the openlayers-elements repo and run
the dev server.

### Create Symlinks

Make local packages accessible for global install:

```bash
git clone https://github.com/zazuko/openlayers-elements
cd openlayers-elements/elements/openlayers-core
pnpm link -g
../openlayers-elements/
pnpm link -g
```

Then return to this repo and run:

```bash
pnpm link -g @openlayers-elements/core
pnpm link -g @openlayers-elements/maps
```

### Update Dependencies

```json
  "dependencies": {
    "@openlayers-elements/core": "link:../openlayers-elements/elements/openlayers-core",
    "@openlayers-elements/maps": "link:../openlayers-elements/elements/openlayers-elements"
  }
```

Then install:

```bash
pnpm install
```

### Run the Dev Server

```bash
pnpm run dev
```

Access on <http://localhost:3000>
